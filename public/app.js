let supportedLanguages = [
    'Abkhazian',
    'Afar',
    'Afrikaans',
    'Akan',
    'Albanian',
    'Amharic',
    'Arabic',
    'Modern Standard Arabic',
    'Maghrebi Arabic',
    'Egyptian Arabic',
    'Saʽidi Arabic',
    'Mesopotamian Arabic',
    'Hejazi Arabic',
    'Gulf Arabic',
    'Yemeni Arabic',
    'Najdi Arabic',
    'Levantine Arabic',
    'Sudanese Arabic',
    'Aragonese',
    'Armenian',
    'Assamese',
    'Avaric',
    'Avestan',
    'Aymara',
    'Azerbaijani',
    'Bambara',
    'Bashkir',
    'Basque',
    'Belarusian',
    'Bengali',
    'Bihari languages',
    'Bislama',
    'Bosnian',
    'Breton',
    'Bulgarian',
    'Burmese',
    'Catalan',
    'Chamorro',
    'Chechen',
    'Chichewa',
    'Chinese',
    'Mandarin',
    'Yue (Cantonese)',
    'Wu (Shanghainese)',
    'Minbei (Fuzhou)',
    'Minnan (Hokkien-Taiwanese)',
    'Chuvash',
    'Cornish',
    'Corsican',
    'Cree',
    'Croatian',
    'Czech',
    'Danish',
    'Divehi',
    'Dutch',
    'Dzongkha',
    'English',
    'Esperanto',
    'Estonian',
    'Ewe',
    'Faroese',
    'Fijian',
    'Finnish',
    'French',
    'Fulah',
    'Galician',
    'Georgian',
    'German',
    'Greek',
    'Guarani',
    'Gujarati',
    'Haitian',
    'Hausa',
    'Hebrew',
    'Herero',
    'Hindi',
    'Hiri Motu',
    'Hungarian',
    'Indonesian',
    'Irish',
    'Igbo',
    'Inupiaq',
    'Icelandic',
    'Italian',
    'Inuktitut',
    'Japanese',
    'Javanese',
    'Kalaallisut',
    'Kannada',
    'Kanuri',
    'Kashmiri',
    'Kazakh',
    'Central Khmer',
    'Kikuyu',
    'Kinyarwanda',
    'Kirghiz',
    'Komi',
    'Kongo',
    'Korean',
    'Kurdish',
    'Kuanyama',
    'Latin',
    'Luxembourgish',
    'Ganda',
    'Limburgan',
    'Lingala',
    'Lao',
    'Lithuanian',
    'Luba-Katanga',
    'Latvian',
    'Manx',
    'Macedonian',
    'Malagasy',
    'Malay',
    'Malayalam',
    'Maltese',
    'Maori',
    'Marathi',
    'Marshallese',
    'Mongolian',
    'Nauru',
    'Navajo',
    'North Ndebele',
    'Nepali',
    'Ndonga',
    'Norwegian',
    'Sichuan Yi',
    'South Ndebele',
    'Occitan',
    'Ojibwa',
    'Oromo',
    'Oriya',
    'Ossetian',
    'Punjabi',
    'Pali',
    'Persian',
    'Polish',
    'Pashto',
    'Portuguese',
    'Quechua',
    'Romansh',
    'Rundi',
    'Romanian',
    'Russian',
    'Sanskrit',
    'Sardinian',
    'Sindhi',
    'Northern Sami',
    'Samoan',
    'Sango',
    'Scots',
    'Scottish Gaelic',
    'Serbian',
    'Shona',
    'Sinhala',
    'Slovak',
    'Slovenian',
    'Somali',
    'Southern Sotho',
    'Spanish',
    'Sundanese',
    'Swahili',
    'Swati',
    'Swedish',
    'Tamil',
    'Telugu',
    'Tajik',
    'Thai',
    'Tigrinya',
    'Tibetan',
    'Turkmen',
    'Tagalog',
    'Tswana',
    'Tonga (Tonga Islands)',
    'Turkish',
    'Tsonga',
    'Tatar',
    'Twi',
    'Tahitian',
    'Uighur',
    'Ukrainian',
    'Urdu',
    'Uzbek',
    'Venda',
    'Vietnamese',
    'Walloon',
    'Welsh',
    'Wolof',
    'Western Frisian',
    'Xhosa',
    'Yiddish',
    'Yoruba',
    'Zhuang',
    'Zul',
];

let supportedCountries = [
    'Not specified',
    'Afghanistan',
    'Albania',
    'Algeria',
    'American Samoa',
    'Andorra',
    'Angola',
    'Anguilla',
    'Antarctica',
    'Antigua & Barbuda',
    'Argentina',
    'Armenia',
    'Aruba',
    'Ascension Island',
    'Australia',
    'Austria',
    'Azerbaijan',
    'Bahamas',
    'Bahrain',
    'Bangladesh',
    'Barbados',
    'Belarus',
    'Belgium',
    'Belize',
    'Benin',
    'Bermuda',
    'Bhutan',
    'Bolivia',
    'Bosnia & Herzegovina',
    'Botswana',
    'Bouvet Island',
    'Brazil',
    'British Indian Ocean Territory',
    'British Virgin Islands',
    'Brunei',
    'Bulgaria',
    'Burkina Faso',
    'Burundi',
    'Cambodia',
    'Cameroon',
    'Canada',
    'Canary Islands',
    'Cape Verde',
    'Caribbean Netherlands',
    'Cayman Islands',
    'Central African Republic',
    'Ceuta & Melilla',
    'Chad',
    'Chile',
    'China',
    'Christmas Island',
    'Clipperton Island',
    'Cocos (Keeling) Islands',
    'Colombia',
    'Comoros',
    'Congo - Brazzaville',
    'Congo - Kinshasa',
    'Cook Islands',
    'Costa Rica',
    'Croatia',
    'Cuba',
    'Curaçao',
    'Cyprus',
    'Czechia',
    'Côte d’Ivoire',
    'Denmark',
    'Diego Garcia',
    'Djibouti',
    'Dominica',
    'Dominican Republic',
    'Ecuador',
    'Egypt',
    'El Salvador',
    'Equatorial Guinea',
    'Eritrea',
    'Estonia',
    'Eswatini',
    'Ethiopia',
    'European Union',
    'Falkland Islands',
    'Faroe Islands',
    'Fiji',
    'Finland',
    'France',
    'French Guiana',
    'French Polynesia',
    'French Southern Territories',
    'Gabon',
    'Gambia',
    'Georgia',
    'Germany',
    'Ghana',
    'Gibraltar',
    'Greece',
    'Greenland',
    'Grenada',
    'Guadeloupe',
    'Guam',
    'Guatemala',
    'Guernsey',
    'Guinea',
    'Guinea-Bissau',
    'Guyana',
    'Haiti',
    'Heard & McDonald Islands',
    'Honduras',
    'Hong Kong SAR China',
    'Hungary',
    'Iceland',
    'India',
    'Indonesia',
    'Iran',
    'Iraq',
    'Ireland',
    'Isle of Man',
    'Israel',
    'Italy',
    'Jamaica',
    'Japan',
    'Jersey',
    'Jordan',
    'Kazakhstan',
    'Kenya',
    'Kiribati',
    'Kosovo',
    'Kuwait',
    'Kyrgyzstan',
    'Laos',
    'Latvia',
    'Lebanon',
    'Lesotho',
    'Liberia',
    'Libya',
    'Liechtenstein',
    'Lithuania',
    'Luxembourg',
    'Macao Sar China',
    'Madagascar',
    'Malawi',
    'Malaysia',
    'Maldives',
    'Mali',
    'Malta',
    'Marshall Islands',
    'Martinique',
    'Mauritania',
    'Mauritius',
    'Mayotte',
    'Mexico',
    'Micronesia',
    'Moldova',
    'Monaco',
    'Mongolia',
    'Montenegro',
    'Montserrat',
    'Morocco',
    'Mozambique',
    'Myanmar (Burma)',
    'Namibia',
    'Nauru',
    'Nepal',
    'Netherlands',
    'New Caledonia',
    'New Zealand',
    'Nicaragua',
    'Niger',
    'Nigeria',
    'Niue',
    'Norfolk Island',
    'North Korea',
    'North Macedonia',
    'Northern Mariana Islands',
    'Norway',
    'Oman',
    'Pakistan',
    'Palau',
    'Palestinian Territories',
    'Panama',
    'Papua New Guinea',
    'Paraguay',
    'Peru',
    'Philippines',
    'Pitcairn Islands',
    'Poland',
    'Portugal',
    'Puerto Rico',
    'Qatar',
    'Romania',
    'Russia',
    'Rwanda',
    'Réunion',
    'Samoa',
    'San Marino',
    'Saudi Arabia',
    'Senegal',
    'Serbia',
    'Seychelles',
    'Sierra Leone',
    'Singapore',
    'Sint Maarten',
    'Slovakia',
    'Slovenia',
    'Solomon Islands',
    'Somalia',
    'South Africa',
    'South Georgia & South Sandwich Islands',
    'South Korea',
    'South Sudan',
    'Spain',
    'Sri Lanka',
    'St. Barthélemy',
    'St. Helena',
    'St. Kitts & Nevis',
    'St. Lucia',
    'St. Martin',
    'St. Pierre & Miquelon',
    'St. Vincent & Grenadines',
    'Sudan',
    'Suriname',
    'Svalbard & Jan Mayen',
    'Sweden',
    'Switzerland',
    'Syria',
    'São Tomé & Príncipe',
    'Taiwan',
    'Tajikistan',
    'Tanzania',
    'Thailand',
    'Timor-Leste',
    'Togo',
    'Tokelau',
    'Tonga',
    'Trinidad & Tobago',
    'Tristan Da Cunha',
    'Tunisia',
    'Turkey',
    'Turkmenistan',
    'Turks & Caicos Islands',
    'Tuvalu',
    'U.S. Outlying Islands',
    'U.S. Virgin Islands',
    'Uganda',
    'Ukraine',
    'United Arab Emirates',
    'United Kingdom',
    'United Nations',
    'United States',
    'Uruguay',
    'Uzbekistan',
    'Vanuatu',
    'Vatican City',
    'Venezuela',
    'Vietnam',
    'Wallis & Futuna',
    'Western Sahara',
    'Yemen',
    'Zambia',
    'Zimbabwe',
    'Åland Islands',
];

let supportedDialects = [
    'Standard German',
    'Austrian German',
    'Bavarian German',
    'Franconian German',
    'Swiss German',
    'Swiss German (Chur)'
];

let supportedProficiencies = [
    'N/A',
    'CEFR A',
    'CEFR B',
    'CEFR C',
    'Native Speaker',
    'Heritage Speaker'
];

$(function() {
    addLanguageMenu(true);
    addDialectMenu(true);

    let origin = $('#country-origin');
    let residence = $('#country-residence');

    for (let country of supportedCountries) {
        origin.append(new Option(country, country));
        residence.append(new Option(country, country));
    }

    $('#copied').hide();
    $('#result-group').hide();
    
    $('#add-language').click(function () {
        addLanguageMenu(false);
    });

    $('#remove-language').click(function () {
        removeEntry('#languages', '#remove-language');
    });
    
    $('#add-dialect').click(function () {
        addDialectMenu(false);
    });

    $('#remove-dialect').click(function () {
       removeEntry('#dialects', '#remove-dialect');
    });

    $('#generate').click(function () {
        generateBotCommand();
    });
});

function removeEntry(selector, buttonSelector) {
    let menu = $(selector);
    let children = menu.children();

    if (children.length > 1) {
        $(children[children.length - 1]).slideToggle('fast', function () {
            $(this).remove();
        });
    }

    // After the removal, only one child is left, so hide the removal button.
    if (children.length === 2) {
        $(buttonSelector).hide();
    }
}

function addLanguageMenu(header) {
    let languages = $('#languages');
    let i = languages.children().length;

    let proficiencies = supportedProficiencies.map(p => `<option value="${p}">${p}</option>`).join('\n');

    $(`
        <div class="row mb-1 mt-1">
            <div class="col-9">
                ${header ? `<label for='language-${i}'>Language</label>` : ''}
                <select class="form-control" id="language-${i}">
                </select>
            </div>
            <div class="col-3">
                ${header ? `<label for="cefr-${i}">Proficiency</label>` : ''}
                <select class="form-control" id="cefr-${i}">
                    ${proficiencies}
                </select>
            </div>
        </div>
    `).hide().appendTo(languages).slideDown('fast');

    let languageMenu = $(`#language-${i}`);
    for (let language of supportedLanguages) {
        languageMenu.append(new Option(language, language));
    }

    if (i === 0) {
        $('#remove-language').hide();
    } else {
        $('#remove-language').show(150);
    }
}

function addDialectMenu(header) {
    let dialects = $('#dialects');
    let i = dialects.children().length;

    let proficiencies = supportedProficiencies.map(p => `<option value="${p}">${p}</option>`).join('\n');

    $(`
    <div class="row mb-1 mt-1">
        <div class="col-9">
            ${header ? `<label for='dialect-${i}'>Dialect</label>` : ''}
            <select class="form-control" id="dialect-${i}">
            </select>
        </div>
        <div class="col-3">
            ${header ? `<label for="d-cefr-${i}">Proficiency</label>` : ''}
            <select class="form-control" id="d-cefr-${i}">
                ${proficiencies}
            </select>
        </div>
    </div>
    `).hide().appendTo(dialects).slideDown('fast');

    let dialectMenu = $(`#dialect-${i}`);
    for (let dialect of supportedDialects) {
        dialectMenu.append(new Option(dialect, dialect));
    }

    if (i === 0) {
        $('#remove-dialect').hide();
    } else {
        $('#remove-dialect').show(150);
    }
}

function extractLanguages(languageSet, groupSelector, languageSelector, cefrSelector) {
    let languages = $(groupSelector).children();
    
    let result = [];
    
    for (let i = 0; i < languages.length; i++) {
        let language = languageSet.indexOf($(`#${languageSelector}-${i}`).val());
        let proficiency = supportedProficiencies.indexOf($(`#${cefrSelector}-${i}`).val());

        // The user tampered with the site's data, so we abort and make them worry about it.
        if (language === -1 || proficiency === -1) {
            return null;
        }


        result.push(`${language}-${proficiency}`);
    }

    return result.join(';');
}

function generateBotCommand() {
    let origin = supportedCountries.indexOf($('#country-origin').val());
    let residence = supportedCountries.indexOf($('#country-residence').val());

    let languages = extractLanguages(supportedLanguages,'#languages', 'language', 'cefr');
    let dialects = extractLanguages(supportedDialects,'#dialects', 'dialect', 'd-cefr');

    let biography = $('#bio').val();

    // Frontend sanity checks.
    if (origin === - 1 || residence === -1 || languages == null || dialects == null ||
        languages === '' || dialects === '' || biography.length > 1024) {
        alert("It looks like you tampered with the site's data. Contact a moderator if this is not the case.")

        return;
    }

    let params = [origin, residence, languages, dialects, biography].join(',');

    $('#result-group').slideDown('fast');
    $('#result').text(`>profile save ${params}`).click(function () {
        $(this).select();
        document.execCommand('copy');

        $('#copied').fadeIn(200).delay(500).fadeOut();
    });
}